.PHONY: up

## Should launch the nginx server.
up:
	sops -d --output docker-compose.override.yml docker-compose.override.encrypted.yml
	docker-compose up