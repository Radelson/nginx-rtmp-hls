# nginx-rtmp-hls

https://blog.cake.sh/2020/10/multistreaming-rtmp-in-the-cloud-using-obs-nginx-and-docker/

1) cloud init
2) docker compose
3) nginx conf

## HOW-TO
### Prod setup

* https://console.hetzner.cloud/
* Create server
  Debian - Nuremberg - local nvme std - CX21
* Add cloud init located in this repo and replace the secret with the correct GPG decrypting key
* Change stream.b.live dns A record to point to the new addr